﻿using Microsoft.AspNetCore.Authorization.Infrastructure;
using Microsoft.AspNetCore.Authorization;
using System.Linq;
using System.Security.Claims;

namespace FPS.API.Attribute
{
    public class RolesAuthorizationHandler : AuthorizationHandler<RolesAuthorizationRequirement>, IAuthorizationHandler
    {
        public static string Status403Forbidden = "Bạn không có quyền thực hiện chức năng này!";
        private readonly IConfiguration _configuration;
        public RolesAuthorizationHandler(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="requirement"></param>
        /// <returns></returns>
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context,
                                                       RolesAuthorizationRequirement requirement)
        {

            if (context.User == null || !context.User.Identity.IsAuthenticated)
            {
                context.Fail();
                return Task.CompletedTask;
            }

            if (requirement.AllowedRoles == null ||
                requirement.AllowedRoles.Any() == false)
            {
                context.Succeed(requirement);
                return Task.CompletedTask;
            }

            var claims = context.User.Claims;
            if (claims == null || claims.Count() == 0)
            {
                context.Fail();
                return Task.CompletedTask;
            }

            var roles = requirement.AllowedRoles;
            var roleClaim = claims.FirstOrDefault(c => c.Type == ClaimTypes.Role);
            if (roleClaim == null || string.IsNullOrEmpty(roleClaim.Value))
            {
                context.Fail();
                return Task.CompletedTask;
            }

            var myRole = roleClaim.Value.Split(";");
            var rs = roles.Intersect(myRole);
            if (rs.Count() > 0)
            {
                context.Succeed(requirement);
                return Task.CompletedTask;
            }
            context.Fail();
            return Task.CompletedTask;
        }
    }
}

