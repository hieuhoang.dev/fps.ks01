﻿using Microsoft.AspNetCore.Authorization;

namespace FPS.API.Attribute
{
    public class CustomRoleAttribute : AuthorizeAttribute
    {
        public static string Status403Forbidden = "Bạn không có quyền thực hiện chức năng này!";
        public CustomRoleAttribute(params string[] roles)
        {
            Roles = String.Join(",", roles);
        }
    }
}
