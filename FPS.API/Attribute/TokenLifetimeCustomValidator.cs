﻿using Microsoft.IdentityModel.Tokens;

namespace FPS.API.Attribute
{
    public static class TokenLifetimeCustomValidator
    {
        public static bool Validate(
        DateTime? notBefore,
        DateTime? expires,
        SecurityToken tokenToValidate,
        TokenValidationParameters @param
    )
        {
            if (expires == null)
                return true;
            if (expires < DateTime.UtcNow)
                return false;
            return true;
        }
    }
}
