﻿using FPS.API.Attribute;
using FPS.Common;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using System.Web;

namespace FPS.API.Controllers
{
    public class BaseController : Controller
    {
        private readonly IConfiguration _configuration;
        public readonly ILogger<BaseController> _logger;
        public BaseController(IConfiguration configuration, ILogger<BaseController> logger)
        {
            _configuration = configuration;
            _logger = logger;
        }

        protected string userId
        {
            get
            {
                var token = HttpContext.Request.Headers["Authorization"].FirstOrDefault()?.Split(" ").Last();
                if (!string.IsNullOrEmpty(token))
                {
                    try
                    {
                        var claims = TokenUtil.ValidateToken(token, _configuration["Tokens:Key"], _configuration["Tokens:Issuer"]);
                        var user = claims.FindFirst(x => x.Type == ClaimTypes.NameIdentifier).Value;
                        return user;

                    }
                    catch (Exception ex)
                    {
                        _logger.LogError(ex, "");
                    }

                }
                return null;
            }
        }
        protected string ip
        {
            get
            {

                try
                {
                    return HttpContext.Connection.RemoteIpAddress.ToString();
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, "");
                }

                return null;
            }
        }

        public static string GetPlainTextAll(string fileName)
        {
            fileName = HttpUtility.HtmlDecode(fileName).Replace(@"-", " ")
                         .Replace(@"\", "").Replace(@"/", "")
                         .Replace("<script>", "").Replace("</script>", "")
                         .Replace("svg", "").Replace("<", "")
                         .Replace(">", "").Replace("@", "")
                         .Replace("#", "").Replace("$", "")
                         .Replace("%", "").Replace("^", "")
                         .Replace("&", "").Replace("*", "")
                         .Replace("{", "").Replace("}", "")
                         .Replace(";", "").Replace("alert", "")
                         .Replace("spam", "").Replace("+", "")
                         .Replace("'", "").Replace("(", "")
                         .Replace(")", "").Replace("\"", "")
                         .Replace("=", "");
            return fileName;
        }
    }
}
