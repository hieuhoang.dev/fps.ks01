﻿using FPS.API.Attribute;
using FPS.BusinessLayer.Repository.Role;
using FPS.Common;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;


namespace FPS.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RoleController : ControllerBase
    {
        private readonly IRoleRepository _roleRepository;
        public RoleController(
            IRoleRepository roleRepository)
        {
            _roleRepository = roleRepository;
        }

        [Route("GetListRole")]
        [HttpGet]
        //[CustomRole(RoleConstant.ADMIN, RoleConstant.ACCOUNTANT_HO, RoleConstant.ACCOUNTANT_BP)]
        public IActionResult GetListRole()
        {
            var roleData = _roleRepository.GetListRole();
            return Ok(roleData);
        }

        //[Route("GetListRoleForCombo")]
        //[HttpGet]
        //[CustomRole(RoleConstant.ADMIN, RoleConstant.ACCOUNTANT_HO, RoleConstant.ACCOUNTANT_BP)]
        //public List<SelectListItem> GetListRoleForCombo()
        //{
        //    return _roleRepository.GetListRoleForCombo();
        //}
    }
}
