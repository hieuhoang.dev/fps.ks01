﻿using FPS.API.Attribute;
using FPS.BusinessLayer.Repository.Role;
using FPS.BusinessLayer.Repository.UserInfo;
using FPS.BusinessLayer.ViewModel.Request;
using FPS.BusinessLayer.ViewModel.Response;
using FPS.BusinessLayer.ViewModel.UserInfo;
using FPS.Common;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FPS.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserInfoController : ControllerBase
    {
        private readonly IUserInfoRepository _userInfoRepository;

        public UserInfoController(
            IUserInfoRepository userInfoRepository)
        {
            _userInfoRepository = userInfoRepository;
        }

        [Route("GetListUser")]
        [HttpGet]
        //[CustomRole(RoleConstant.ADMIN)]
        public TableResponse<UserViewModel> GetListUser(SearchUserModel search)
        {
            return _userInfoRepository.GetListUser(search);
        }

        [Route("CreateUser")]
        [HttpPost]
        //[CustomRole(RoleConstant.ADMIN)]
        public Response<string> CreateUser(UserModel model)
        {
            var rs = _userInfoRepository.CreateUser(model);
            return rs;
        }
        [Route("GetUserById")]
        [HttpGet]
        //[CustomRole(RoleConstant.ADMIN, RoleConstant.ACCOUNTANT_HO, RoleConstant.ACCOUNTANT_BP)]
        public Response<UserModel> GetUserById(UserModel model)
        {
            return _userInfoRepository.GetUserById(model);
        }

        [Route("UpdateUser")]
        [HttpPost]
        //[CustomRole(RoleConstant.ADMIN)]
        public Response<string> UpdateUser(UserModel model)
        {
            var rs = _userInfoRepository.UpdateUser(model);
            return rs;
        }

        [Route("DeleteUser")]
        [HttpPost]
        //[CustomRole(RoleConstant.ADMIN)]
        public Response<string> DeleteUser(UserModel model)
        {
            var rs = _userInfoRepository.DeleteUser(model);
            return rs;
        }

        [Route("GetUserByUserName")]
        [HttpGet]
        //[CustomRole(RoleConstant.ADMIN, RoleConstant.ACCOUNTANT_HO, RoleConstant.ACCOUNTANT_BP)]
        public Response<UserViewModel> GetUserByUserName(UserViewModel model)
        {
            return _userInfoRepository.GetUserByUserName(model);
        }
    }
}
