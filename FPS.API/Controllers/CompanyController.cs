﻿using FPS.BusinessLayer.Repository.Company;
using FPS.BusinessLayer.ViewModel.Company;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FPS.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CompanyController : ControllerBase
    {
        private readonly ICompanyRepository _companyRepository;

        public CompanyController(ICompanyRepository companyRepository)
        {
            _companyRepository = companyRepository;
        }

        [HttpPost]
        [Route("AddCompany")]
        public void AddCompany(CompanyModel model)
        {
            _companyRepository.AddCompany(model);
        }

        [HttpGet]
        [Route("GetAllCompany")]
        public List<CompanyViewModel> GetAllCompany() { 
            return _companyRepository.GetAllCompany();
        }
    }
}
