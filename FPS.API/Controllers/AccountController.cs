﻿using FPS.BusinessLayer.Repository.Account;
using FPS.BusinessLayer.Repository.Role;
using FPS.BusinessLayer.ViewModel.Response;
using FPS.BusinessLayer.ViewModel.UserInfo;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FPS.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly IAccountRepository _accountRepository;
        private readonly IRoleRepository _roleRepository;

        public AccountController(
            IAccountRepository accountRepository,
            IRoleRepository roleRepository
            )
        {
            _accountRepository = accountRepository;
            _roleRepository = roleRepository;
        }

        [Route("Authencate")]
        [HttpPost]
        public Response<string> Authencate(LoginModel model)
        {
            Response<string> res = new Response<string>();

            var rs = _accountRepository.Authencate(model);
            if (rs.Code != StatusCodes.Status200OK)
            {
                res.Code = StatusCodes.Status401Unauthorized;
                res.Message = rs.Message;
                return res;
            }
            var role = _roleRepository.GetRoleByUserId(rs.Data.Id);
            //var newToken = _accountRepository.EncodeSha256(rs.Data, role.Data.UserRoleName);
            //res.Data = newToken;
            if (!string.IsNullOrEmpty(model.Token))
                res.Code = StatusCodes.Status200OK;
            return res;
        }
    }
}
