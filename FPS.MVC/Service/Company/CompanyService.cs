﻿using FPS.BusinessLayer.ViewModel.Company;
using FPS.MVC.ApiConfig;

namespace FPS.MVC.Service.Company
{
    public class CompanyService : BaseService, ICompanyService
    {
        public CompanyService(HttpClient client) : base(client)
        {
        } 

        public List<CompanyViewModel> GetAllCompany()
        {
            var response = _client.GetAsync(PathConfig.GET_ALL_COMPANY);
            return null;
        }
    }
}
