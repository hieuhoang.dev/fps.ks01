﻿namespace FPS.MVC.Service
{
    public class BaseService
    {
        public readonly HttpClient _client;

        public BaseService(HttpClient client)
        {
            _client = client;
        }
    }
}
