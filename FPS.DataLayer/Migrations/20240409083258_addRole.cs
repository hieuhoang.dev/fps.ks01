﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace FPS.DataLayer.Migrations
{
    /// <inheritdoc />
    public partial class addRole : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Role",
                columns: new[] { "Id", "ConcurrencyStamp", "CreatedBy", "CreatedDate", "IsDeleted", "Name", "NormalizedName", "UpdatedBy", "UpdatedDate" },
                values: new object[,]
                {
                    { new Guid("71bae7ad-a1fe-4010-a807-0539e92c0876"), null, null, null, false, "admin", "Quản trị", null, null },
                    { new Guid("88a351cc-2e14-4608-9d5d-d635a62b1ecc"), null, null, null, false, "accountant_bp", "Bộ phận", null, null },
                    { new Guid("a15fc933-90fe-40be-a882-7aeda75a0c9c"), null, null, null, false, "accountant_ho", "Kế toán", null, null }
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Role",
                keyColumn: "Id",
                keyValue: new Guid("71bae7ad-a1fe-4010-a807-0539e92c0876"));

            migrationBuilder.DeleteData(
                table: "Role",
                keyColumn: "Id",
                keyValue: new Guid("88a351cc-2e14-4608-9d5d-d635a62b1ecc"));

            migrationBuilder.DeleteData(
                table: "Role",
                keyColumn: "Id",
                keyValue: new Guid("a15fc933-90fe-40be-a882-7aeda75a0c9c"));
        }
    }
}
