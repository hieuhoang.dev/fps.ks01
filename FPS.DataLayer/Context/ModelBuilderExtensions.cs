﻿using FPS.Common;
using FPS.DataLayer.Entity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPS.DataLayer.Context
{
    public static class ModelBuilderExtensions
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {
            // any guid, but nothing is against to use the same one
            var accountantHoRoleId = Guid.NewGuid();
            var accountantBpRoleId = Guid.NewGuid();
            var adminRoleId = Guid.NewGuid();
            modelBuilder.Entity<Role>().HasData(new Role
            {
                Id = adminRoleId,
                Name = RoleConstant.ADMIN,
                NormalizedName = "Quản trị"
            });
            modelBuilder.Entity<Role>().HasData(new Role
            {
                Id = accountantHoRoleId,
                Name = RoleConstant.ACCOUNTANT_HO,
                NormalizedName = "Kế toán"
            });
            modelBuilder.Entity<Role>().HasData(new Role
            {
                Id = accountantBpRoleId,
                Name = RoleConstant.ACCOUNTANT_BP,
                NormalizedName = "Bộ phận",
            });
        }
    }
}
