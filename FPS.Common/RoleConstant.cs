﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPS.Common
{
    public class RoleConstant
    {
        public const string ADMIN = "admin";
        public const string ACCOUNTANT_HO = "accountant_ho";
        public const string ACCOUNTANT_BP = "accountant_bp";
    }
}
