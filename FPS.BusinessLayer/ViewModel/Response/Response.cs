﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPS.BusinessLayer.ViewModel.Response
{
    public class Response<T>
    {
        public Response()
        {
            Code = 200;
        }
        public int Code { get; set; }
        public string Message { get; set; }
        //public string DataError { get; set; }
        public T Data { get; set; }
        public List<T> DataList { get; set; }
    }
}
