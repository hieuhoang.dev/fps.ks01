﻿using FPS.BusinessLayer.ViewModel.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPS.BusinessLayer.ViewModel.Response
{
    public class TableResponse<T>
    {
        public TableResponse()
        {
            Code = 200;
            RecordsTotal = RecordsFiltered = 0;
            Data = new List<T>();
        }
        public int Code { get; set; }
        public Boolean Success { get; set; }
        public string Message { get; set; }
        public string DataError { get; set; }
        public int Draw { get; set; }
        public int RecordsTotal { get; set; }
        public int RecordsFiltered { get; set; }
        public List<T> Data { get; set; }
        public void setDraw(TableSearchModel search)
        {
            Draw = search.Draw;
        }
    }
}
