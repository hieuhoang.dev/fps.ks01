﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPS.BusinessLayer.ViewModel.Request
{
    public class SearchUserModel : TableSearchModel
    {
        public string Name { get; set; }
    }
}
