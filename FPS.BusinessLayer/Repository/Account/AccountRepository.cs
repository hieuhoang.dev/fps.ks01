﻿using FPS.BusinessLayer.ViewModel.Response;
using FPS.BusinessLayer.ViewModel.UserInfo;
using FPS.Common;
using FPS.DataLayer.Context;
using FPS.DataLayer.Entity;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace FPS.BusinessLayer.Repository.Account
{
    public class AccountRepository : BaseRepository, IAccountRepository
    {
        private readonly IConfiguration _configuration;

        public AccountRepository(FPSContext context, IConfiguration configuration) : base(context)
        {
            _configuration = configuration;
        }

        public string EncodeSha256(User user, string role)
        {
            string token = null;
            var key = _configuration["Tokens:Key"];
            var issuer = _configuration["Tokens:Issuer"];
            var expires = _configuration["Tokens:Expires"];
            if (string.IsNullOrEmpty(key))
                key = "ASDFGHJKLZXCVBNMQWERTYUIOP";
            if (string.IsNullOrEmpty(issuer))
                issuer = "FPS";
            if (string.IsNullOrEmpty(expires))
                expires = "1*24*60*60";
            var arr = expires.Split('*');
            double seconds = 1;
            try
            {
                foreach (var item in arr)
                {
                    seconds *= Int32.Parse(item.ToString());
                }
            }
            catch (Exception ex)
            {
                seconds = 1 * 24 * 60 * 60;
            }
            List<Claim> claims = new List<Claim>();
            claims.Add(new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()));
            claims.Add(new Claim(ClaimTypes.Role, role == null ? "" : role));
            token = TokenUtil.EncodeSha256(claims, key, issuer, seconds);
            return token;
        }

        public Response<User> Authencate(LoginModel model)
        {
            Response<User> res = new Response<User>();
            try
            {
                var user = _context.Users.Where(x => x.UserName == model.UserName && x.IsDeleted == false).FirstOrDefault();
                if (user == null)
                {
                    res.Code = 404;
                    res.Message = "Tài khoản hoặc mật khẩu bị sai!";
                    res.Data = user;
                    return res;
                }
                res.Code =200;
                res.Message = "Đăng nhập thành công!";
                res.Data = user;
                return res;

            }
            catch (Exception ex)
            {
                res.Code = 500;
            }
            return res;
        }
    }
}
