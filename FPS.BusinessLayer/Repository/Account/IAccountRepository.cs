﻿using FPS.BusinessLayer.ViewModel.Response;
using FPS.BusinessLayer.ViewModel.UserInfo;
using FPS.DataLayer.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPS.BusinessLayer.Repository.Account
{
    public interface IAccountRepository
    {
        string EncodeSha256(User user, string role);

        Response<User> Authencate(LoginModel model);
    }
}
