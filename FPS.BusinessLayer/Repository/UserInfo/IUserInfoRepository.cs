﻿using FPS.BusinessLayer.ViewModel.Request;
using FPS.BusinessLayer.ViewModel.Response;
using FPS.BusinessLayer.ViewModel.UserInfo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace FPS.BusinessLayer.Repository.UserInfo
{
    public interface IUserInfoRepository
    {
        TableResponse<UserViewModel> GetListUser(SearchUserModel search);
        Response<string> CreateUser(UserModel model);
        Response<UserModel> GetUserById(UserModel model);
        Response<string> UpdateUser(UserModel model);
        Response<string> DeleteUser(UserModel model);
        Response<UserViewModel> GetUserByUserName(UserViewModel model);
        List<SelectListItem> GetListUserForCombo();
        List<UserViewModel> GetAllUser();
    }
}
