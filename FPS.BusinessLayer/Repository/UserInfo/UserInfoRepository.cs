﻿using FPS.BusinessLayer.ViewModel.Request;
using FPS.BusinessLayer.ViewModel.Response;
using FPS.BusinessLayer.ViewModel.UserInfo;
using FPS.Common;
using FPS.DataLayer.Context;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace FPS.BusinessLayer.Repository.UserInfo
{
    public class UserInfoRepository : BaseRepository, IUserInfoRepository
    {
        public UserInfoRepository(FPSContext context) : base(context)
        {
        }

        public TableResponse<UserViewModel> GetListUser(SearchUserModel search)
        {
            var result = new TableResponse<UserViewModel>();
            result.Draw = search.Draw;
            try
            {
                var query = (from a in _context.Users
                             join b in _context.UserRoles on a.Id equals b.UserId into b2
                             from b1 in b2.DefaultIfEmpty()
                             join c in _context.Roles on b1.RoleId equals c.Id into c2
                             from c1 in c2.DefaultIfEmpty()
                             where a.IsDeleted == false
                             orderby a.CreatedDate descending
                             select new
                             {
                                 Id = a.Id,
                                 UserName = a.UserName,
                                 FullName = a.FullName,
                                 UserRoleName = c1.NormalizedName
                             }).ToList();

                var group = query.GroupBy(x => x.Id).Select(group => new UserViewModel
                {
                    Id = group.Key,
                    UserName = group.FirstOrDefault().UserName,
                    FullName = group.FirstOrDefault().FullName,
                    UserRoleName = group.FirstOrDefault().UserRoleName
                }).ToList();

                if (search.SearchValue != null)
                {
                    group = group.Where(x => x.FullName.ToLower().Contains(search.SearchValue.ToLower()) || x.UserName.ToLower().Contains(search.SearchValue.ToLower())).ToList();
                }

                var cnt = group.Count();
                result.Data = group.Skip(search.Start).Take(search.Length).ToList();
                result.RecordsTotal = cnt;
                result.RecordsFiltered = cnt;
                result.Code = 200;
            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = "Xảy ra lỗi khi lấy danh sách người dùng!";
            }
            return result;
        }

        public Response<string> CreateUser(UserModel model)
        {
            Response<string> res = new Response<string>();
            try
            {
                if (model.RoleName == null)
                {
                    res.Code = 400;
                    res.Message = "Nhóm quyền không được bỏ trống!";
                    return res;
                }
                var roleData = _context.Roles.Where(x => x.Name == model.RoleName);
                if (roleData.Count() == 0)
                {
                    res.Code = 400;
                    res.Message = "Nhóm quyền không tồn tại!";
                    return res;
                }
                else
                {
                    model.RoleId = roleData.FirstOrDefault().Id;
                }
                var userData = _context.Users.FirstOrDefault(x => x.IsDeleted == false && x.UserName == model.UserName.Trim());
                if (userData != null)
                {
                    res.Code = 404;
                    res.Message = "Người dùng đã tồn tại!";
                    return res;
                }
                var hasher = new PasswordHasher<UserModel>();
                var userId = Guid.NewGuid();
                FPS.DataLayer.Entity.User user = new FPS.DataLayer.Entity.User();
                user.Id = userId;
                user.UserName = model.UserName.TrimEnd().TrimStart().ToLower();
                user.PasswordHash = hasher.HashPassword(null, LoginConstant.P_AD_DEFAUL);
                user.FullName = model.FullName;
                user.CreatedBy = model.CreatedBy;
                user.CreatedDate = DateTime.Now;
                user.UpdatedBy = model.CreatedBy;
                user.UpdatedDate = DateTime.Now;
                user.NormalizedEmail = model.UserName.TrimEnd().TrimStart().ToLower() + "@fpt.com"; ;
                user.PhoneNumberConfirmed = false;
                user.TwoFactorEnabled = false;
                user.IsDeleted = false;
                _context.Users.Add(user);

                var _userRole = new IdentityUserRole<Guid>
                {
                    RoleId = model.RoleId,
                    UserId = userId
                };
                _context.UserRoles.Add(_userRole);

                _context.SaveChanges();
                res.Code = 200;
                res.Message = "Thêm người dùng thành công!";
                return res;
            }
            catch (Exception ex)
            {
                res.Code = 500;
                res.Message = "Xảy ra lỗi khi thêm người dùng!";
            }
            return res;
        }

        public Response<UserModel> GetUserById(UserModel model)
        {
            Response<UserModel> res = new Response<UserModel>();
            try
            {
                var query = (from a in _context.Users
                             join b in _context.UserRoles on a.Id equals b.UserId into b2
                             from b1 in b2.DefaultIfEmpty()
                             join c in _context.Roles on b1.RoleId equals c.Id into c2
                             from c1 in c2.DefaultIfEmpty()
                             where a.IsDeleted == false && a.Id == model.Id
                             select new
                             {
                                 a,
                                 b1,
                                 c1
                             }).ToList();
                var group = query.GroupBy(x => x.a.Id).Select(group => new UserModel
                {
                    Id = group.Key,
                    UserName = group.ToList().FirstOrDefault().a.UserName,
                    FullName = group.ToList().FirstOrDefault().a.FullName,
                    RoleId = group.ToList().FirstOrDefault() != null ? group.ToList().FirstOrDefault().c1.Id : new Guid(),
                    RoleName = group.ToList().FirstOrDefault().c1.Name,
                }).ToList();
                res.Code = 200;
                res.Data = group.FirstOrDefault();
                return res;
            }
            catch (Exception ex)
            {
                res.Code = 400;
                res.Message = "Xảy ra lỗi khi lấy thông tin người dùng!";
                return res;
            }
        }

        public Response<string> UpdateUser(UserModel model)
        {
            Response<string> res = new Response<string>();
            try
            {
                if (model.RoleName == null)
                {
                    res.Code = 400;
                    res.Message = "Nhóm quyền không được bỏ trống!";
                    return res;
                }
                var roleData = _context.Roles.Where(x => x.Name == model.RoleName);
                if (roleData.Count() == 0)
                {
                    res.Code = 400;
                    res.Message = "Nhóm quyền không tồn tại!";
                    return res;
                }
                else
                {
                    model.RoleId = roleData.FirstOrDefault().Id;
                }

                res.Code = 200;
                res.Message = "Cập nhật người dùng thành công!";
                return res;
            }
            catch (Exception ex)
            {
                res.Code = 500;
                res.Message = "Xảy ra lỗi khi cập nhật người dùng!";
            }
            return res;
        }

        public Response<string> DeleteUser(UserModel model)
        {
            Response<string> res = new Response<string>();
            try
            {
                var user = _context.Users.FirstOrDefault(x => x.Id == model.Id && x.IsDeleted == false);

                if (user == null)
                {
                    res.Code = 400;
                    res.Message = "Không tồn tại người dùng, không thể xóa!";
                    return res;
                }
                user.IsDeleted = true;
                user.UpdatedBy = model.UpdatedBy;
                user.UpdatedDate = DateTime.Now;
                _context.Users.Update(user);
                _context.SaveChanges();

                res.Code = 200;
                res.Message = "Xóa người dùng thành công!";
                return res;
            }
            catch (Exception ex)
            {
                res.Code = 500;
                res.Message = "Xảy ra lỗi khi xóa người dùng!";
            }
            return res;
        }

        public Response<UserViewModel> GetUserByUserName(UserViewModel model)
        {
            Response<UserViewModel> res = new Response<UserViewModel>();
            try
            {
                var user = _context.Users.Where(x => x.UserName == model.UserName && x.IsDeleted == false).FirstOrDefault();
                if (user == null)
                {
                    res.Code = 404;
                    res.Message = "Không tìm thấy người dùng!";
                    return res;
                }
                else
                {
                    var userRole = _context.UserRoles.FirstOrDefault(x => x.UserId == user.Id);
                    var userRoleName = "";
                    var userRoleNormalizedName = "";
                    if (userRole != null)
                    {
                        var dataUserRole = _context.Roles.FirstOrDefault(x => x.Id == userRole.RoleId);
                        userRoleName = dataUserRole.Name;
                        userRoleNormalizedName = dataUserRole.NormalizedName;
                    }

                    res.Code = 200;
                    res.Data = new UserViewModel()
                    {
                        Id = user.Id,
                        UserName = user.UserName,
                        FullName = user.FullName,
                        UserRoleName = userRoleName
                    };
                    return res;
                }

            }
            catch (Exception ex)
            {
                res.Code = 400;
                res.Message = "Xảy ra lỗi khi lấy thông tin người dùng!";
                return res;
            }
        }
        public List<SelectListItem> GetListUserForCombo()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            try
            {
                var _data = _context.Users.Where(x => x.IsDeleted == false).Select(costType => new SelectListItem
                {
                    Value = costType.Id.ToString(),
                    Text = costType.UserName
                }).ToList();
                list = _data;
            }
            catch (Exception ex)
            {
               
            }
            return list;
        }

        public List<UserViewModel> GetAllUser()
        {
            var result = new List<UserViewModel>();
            var _data = _context.Users.Where(x => x.IsDeleted == false);
            try
            {
                if (_data != null && _data.ToList().Count > 0)
                {
                    foreach (var item in _data)
                    {
                        result.Add(new UserViewModel()
                        {
                            Id = item.Id,
                            UserName = item.UserName
                        });
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
            }
            return result;
        }

    }
}
