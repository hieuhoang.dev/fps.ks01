﻿using FPS.BusinessLayer.ViewModel.Response;
using FPS.BusinessLayer.ViewModel.Role;
using FPS.BusinessLayer.ViewModel.UserInfo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace FPS.BusinessLayer.Repository.Role
{
    public interface IRoleRepository
    {
        TableResponse<RoleViewModel> GetListRole();
        List<SelectListItem> GetListRoleForCombo();
        Response<UserViewModel> GetRoleByUserId(Guid userId);
    }
}
