﻿using FPS.BusinessLayer.ViewModel.Response;
using FPS.BusinessLayer.ViewModel.Role;
using FPS.BusinessLayer.ViewModel.UserInfo;
using FPS.DataLayer.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace FPS.BusinessLayer.Repository.Role
{
    public class RoleRepository : BaseRepository, IRoleRepository
    {
        public RoleRepository(FPSContext context) : base(context)
        {
        }

        public TableResponse<RoleViewModel> GetListRole()
        {
            var result = new TableResponse<RoleViewModel>();
            try
            {
                var _data = _context.Roles.Select(role => new RoleViewModel
                {
                    Id = role.Id,
                    Name = role.Name,
                    NormalizedName = role.NormalizedName,
                    ConcurrencyStamp = role.ConcurrencyStamp,
                }).ToList();
                result.Data = _data;
                result.Code = 200;
            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = "Xảy ra lỗi khi lấy danh sách quyền!";
            }

            return result;
        }

        public List<SelectListItem> GetListRoleForCombo()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            try
            {
                var _data = _context.Roles.Select(role => new SelectListItem
                {
                    Value = role.Name,
                    Text = role.NormalizedName
                }).ToList();
                list = _data;
            }
            catch (Exception ex)
            {
            }
            return list;
        }

        public Response<UserViewModel> GetRoleByUserId(Guid userId)
        {
            Response<UserViewModel> res = new Response<UserViewModel>();
            try
            {
                var user = _context.Users.Where(x => x.Id == userId && x.IsDeleted == false).FirstOrDefault();
                if (user == null)
                {
                    res.Code = 404;
                    res.Message = "Không tìm thấy người dùng!";
                    return res;
                }
                else
                {
                    var userRole = _context.UserRoles.FirstOrDefault(x => x.UserId == user.Id);
                    var userRoleName = "";
                    var userRoleNormalizedName = "";
                    if (userRole != null)
                    {
                        var dataUserRole = _context.Roles.FirstOrDefault(x => x.Id == userRole.RoleId);
                        userRoleName = dataUserRole.Name;
                        userRoleNormalizedName = dataUserRole.NormalizedName;
                    }

                    res.Code = 200;
                    res.Data = new UserViewModel()
                    {
                        Id = user.Id,
                        UserRoleName = userRoleName,
                        UserRoleNormalizedName = userRoleNormalizedName
                    };
                    return res;
                }

            }
            catch (Exception ex)
            {
                res.Code = 400;
                res.Message = "Xảy ra lỗi khi lấy thông tin người dùng!";
                return res;
            }
        }
    }
}
