﻿using FPS.BusinessLayer.ViewModel.Company;
using FPS.DataLayer.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPS.BusinessLayer.Repository.Company
{
    public class CompanyRepository : BaseRepository, ICompanyRepository
    {
        public CompanyRepository(FPSContext context) : base(context)
        {
        }

        public void AddCompany(CompanyModel model)
        {
            FPS.DataLayer.Entity.Company company = new FPS.DataLayer.Entity.Company();
            company.Id = Guid.NewGuid();
            company.CompanyName = model.CompanyName;
            company.CreatedDate = DateTime.Now;
            company.UpdatedDate = DateTime.Now;
            company.CreatedBy = model.CreatedBy;
            company.UpdatedBy = model.UpdatedBy;
            company.IsDeleted = false;
            _context.Companies.Add(company);
            _context.SaveChanges();
        }

        public List<CompanyViewModel> GetAllCompany()
        {
            var data = _context.Companies.Where(x => x.IsDeleted == false).Select(x => new CompanyViewModel
            {
                Id = x.Id,
                CompanyName = x.CompanyName
            }).ToList();

            return data;

        }
    }
}
